@extends('layouts.admin')
@section('title', 'Senarai Pengguna')
@section('content')
<h1>Senarai Pengguna</h1>
<br>
<form action="/carian-pengguna">
    Nama Pengguna <input type="text" name="name" value="{{$name}}">
    
    <br>
    <button type="submit" class="btn btn-primary">Teruskan</button>
    <a href="/senarai-pengguna"><button type="button" class="btn btn-danger">Set Semula</button></a>
</form>
<br>
<br>
<a href="/tambah-pengguna"><button class="btn btn-info">Tambah</button></a>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Nama pengguna</th>
        <th>Emel pengguna</th>
        <th>Tindakan</th>
    </tr>
    @php 
    $i = $muser->firstItem();
    @endphp
    @foreach($muser as $data)

        <tr>
            <td>{{$i++}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->email}}</td>
            <td>
            
            <a href="/kemaskini-pengguna/{{$data->id}}"><button class="btn btn-success">Kemaskini</button></a>  
            <form action="/hapus-pengguna/{{$data->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" onclick="return confirm('Anda pasti?')" class="btn btn-danger">Hapus</button>
            </form>            
            </td>
        </tr>

    @endforeach
    

</table>
{{ $muser->links() }}
@endsection