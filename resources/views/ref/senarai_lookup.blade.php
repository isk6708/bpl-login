@extends('layouts.admin')
@section('title', 'Senarai Jadual Rujukan')
@section('content')
<h1>Senarai Jadual Rujukan</h1>
<br>
<form action="/ref">
    Penerangan<input type="text" name="penerangan" value="{{$penerangan}}">
    <button type="submit" class="btn btn-primary">Teruskan</button>
    <a href="/ref"><button type="button" class="btn btn-danger">Set Semula</button></a>
</form>
<br>
<br>
<a href="/ref/create"><button class="btn btn-info">Tambah</button></a>
<table class="table table-striped">
    <tr>
        <th>Bil</th>
        <th>Kod</th>
        <th>Penerangan</th>
        <th>Tindakan</th>
    </tr>
    @php 
    $i = $mref->firstItem();
    @endphp
    @foreach($mref as $data)

        <tr>
            <td>{{$i++}}</td>
            <td>{{$data->kod}}</td>
            <td>{{$data->penerangan}}</td>
            <td>
            <a href="/ref/{{$data->id}}/edit"><button class="btn btn-success">Kemaskini</button></a>  
            <form action="/ref/{{$data->id}}" method="POST">
                @csrf
                @method('DELETE')
                <button type="submit" onclick="return confirm('Anda pasti?')" class="btn btn-danger">Hapus</button>
            </form>            
            </td>
        </tr>

    @endforeach
    

</table>
{{ $mref->links() }}
@endsection