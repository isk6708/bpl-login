@extends('layouts.admin')
@section('title', 'Borang Jadual Rujukan')
@section('content')
<h1>Borang Jadual rujukan</h1>
<br>
@if(!empty($mref->id))
<form action="/ref/{{$mref->id}}" method="POST">
    @method('PUT')
@else
<form action="/ref" method="POST">
@endif

    @csrf
    Kod<input type="text" name="kod" value="{{$mref->kod}}">
    <br>
    Penerangan <input type="text" name="penerangan" value="{{$mref->penerangan}}">
    <br>
    
    <button type="submit">Simpan</button>
    <a href="/ref"><button type="button">Kembali</button></a>
</form>
@endsection