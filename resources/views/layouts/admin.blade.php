<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Demo Bahagian Pengurusan Latihan</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">BPL</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item">
            <a class="nav-link" aria-current="page" href="/home">Utama</a>
            </li>
            @auth
            <li class="nav-item">
            <a class="nav-link" href="/senarai-produk">Produk</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/ref">Rujukan</a>
            </li>
            <li class="nav-item">
            <a class="nav-link" href="/senarai-pengguna">Senarai Pengguna</a>
            </li>
            @endauth
            
            @guest
            <li class="nav-item">

            <a class="nav-link" href="{{ route('login') }}">Log Masuk</a>
            </li>
            @endguest
            @auth
            <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"
                  onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                  Log Keluar
              </a>

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
              </form>
              
            </li>
            @endauth
            
            @guest
            <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Pengguna</a>
            </li>
            @endguest
            
        </ul>
        </div>
    </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js"></script>
  </body>
</html>