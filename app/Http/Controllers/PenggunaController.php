<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class PenggunaController extends Controller
{
    public function senaraiPengguna(Request $req){
        $name = $req->name;
        
        
        $muser = User::where(
            function($query) use ($name){
                if (!empty($name)){
                    $query->where('name','like','%'.$name.'%');
                }
            }
        )
        ->paginate(6);
        
        return view('pengguna.senarai_pengguna',compact('muser','name'));
    }
}
