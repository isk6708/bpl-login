<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Ref;

class ProductController extends Controller
{
    public function senaraiProduk(Request $req){
        $product_name = $req->product_name;
        $product_cat = $req->product_cat;
        
        $mprod = Product::where(
            function($query) use ($product_name){
                if (!empty($product_name)){
                    $query->where('product_name','like','%'.$product_name.'%');
                    // $query->orWhere('product_cat',$product_name);
                }
            }
        )
        ->where(
            function($query) use ($product_cat){
                if (!empty($product_cat)){
                    $query->where('product_cat','=',$product_cat);
                }
            }
        )
        ->paginate(6);
        
        $categories = Ref::all();

        return view('produk.senarai_produk',compact('mprod','product_name','categories','product_cat'));
    }

    public function borangProduk($id=''){
        if (!empty($id)){
            $mprod = Product::find($id);
        }else{
            $mprod = new Product();
        }
        $categories = Ref::all();
        return view('produk.borang_produk',compact('mprod','categories'));
    }

    public function simpanProduk(Request $req,$id=''){
        if (!empty($id)){
            $mprod = Product::find($id);
        }else{
            $mprod = new Product();
        }
        // dd($req);
        $req->validate([
            'product_name'=>'required',
            'product_cat'=>'required'
        ]);

        $mprod->product_name = $req->product_name;
        $mprod->product_cat = $req->product_cat;
        $mprod->save();
        return redirect()->route('product.list');//redirect using named route
    }

    public function hapusProduk($id){
        Product::find($id)->delete();
        return redirect()->route('product.list');
    }

}
