<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ProductController;
use App\Http\Controllers\RefController;
use App\Http\Controllers\PenggunaController;

Route::middleware(['auth'])->get('/senarai-pengguna', [PenggunaController::class,'senaraiPengguna'])->name('user.list');

Route::middleware(['auth'])->get('/senarai-produk', [ProductController::class,'senaraiProduk'])->name('product.list');
Route::middleware(['auth'])->get('/carian-produk', [ProductController::class,'senaraiProduk']);
Route::middleware(['auth'])->get('/tambah-produk', [ProductController::class,'borangProduk'])->name('product.add');
Route::middleware(['auth'])->get('/kemaskini-produk/{id}', [ProductController::class,'borangProduk']);
Route::middleware(['auth'])->post('/simpan-produk/{id?}', [ProductController::class,'simpanProduk']);
Route::middleware(['auth'])->delete('/hapus-produk/{id}', [ProductController::class,'hapusProduk']);

Route::middleware(['auth'])->Resource('ref', RefController::class);

Route::get('/', function () {
    return view('selamat_datang');
});

Route::get('/home', function () {
    return view('selamat_datang');
});

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
